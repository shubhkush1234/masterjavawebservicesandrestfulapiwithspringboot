Introduction to Web Services
::::::::::::::::::::::::::::
1. Service definition:
        request response format
        request structure
        response structure
        end point

2. request response format/message exchane format
    JSON or XML

3.  service provider or server
    service consumer or client
    service definition

4.  transport:
    It defines how a service is called.
    HTTP and MQ
    HTTP: over internet
    MQ: Over a queue. Eg. Websphere MQ, Rabbit MQ etc

5. Web Services groups:
    REST
    SOAP

6. SOAP:
    XML is used. 
    SOAP Envelope, header, body.
    Format:
        SOAP XML request and response
    Transport:
        SOAP over HTTP and MQ
    Service definition:
        WSDL
    
7. WSDL:
    Endpoint
    All Operations
    Request structure
    response structure

8. REST : Representational State Transfer
    Makes best use of HTTP
    HTTP methods: GET PUT POST etc
    HTTP Status code: 200 400 404 etc
    GET: to get something
    POST: to create something
    PUT: to update something

    Eg: creste a user: POST/user
        delete a user: DELETE/users/1
        get all users: GET/users
        get one user: GET/user/1

    Key Abstraction: Resource
        A resource have an URI
            eg: /user/profile
            eg. /user/shubhkush1234/
        A resource can have different represenntations:
            XML
            HTML
            JSON
    REST : 
        Data transfer format:
            no restricitions, JSON(popular)
        Transport:
            Only HTTP
        Service definition:
            No Standard. WADL, Swagger(popular) etc.

Getting Started with Spring Framework
:::::::::::::::::::::::::::::::::::::

1. Creste a Spring project with start.spring.io 
   It's called Srping Initializr
2. Generate a Maven project with Java and Spring Boot 2.0.0
3. group Id: com.shubham.spring
4. artifact id: spring-masterclass
5. then click on Generate Project button. 
    It will download a zip(project). We need to unzip and use it as our project.
 